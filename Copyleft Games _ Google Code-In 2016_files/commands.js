$(document).ready(function() {

    // Init console
    consoleManagement.init();

    // Init pastebin

    $("body").append('<input type="file" id="imgchooser" accept="image/jpeg, image/png" style="display: none">');
    $("#imgchooser").on("change", function() {
        pastebin.pasteImage(this.files[0]);
    });
    pastebin.init("https://media.copyleftgames.org/upload/codebin/",
        "https://media.copyleftgames.org/upload/imgbin/");

    //pastebin.init("http://localhost", "8123");

    $(Candy).on("candy:view.room.after-show", function(e) {
        $(document).on('keydown', 'textarea[name="message"]', function(e) {
            if (e.keyCode == 13 && !(e.shiftKey)) {
                var message = $('textarea[name="message"]:visible').val();
                if (message.indexOf("\n") != -1) {
                    pastebin.paste(message);
                    return false;
                }
                var msg = $('textarea[name="message"]:visible').val();
                var e = {};
                e.message = msg;
                if (beforeSend(e)) {
                    pastebin.sendMessage(msg);
                }
                $('textarea[name="message"]:visible').val("");
                return false;
            }
        });

        $('input[name="message"]').replaceWith(
            '<textarea name="message" class="field" type="text" aria-label="Message Form Text Field" autocomplete="off">'
        );

        if ($("#chat-toolbar").find(".icon-camera").length === 0) {
            $("#chat-toolbar").append('<li class="upload-btn"><span class="icon-camera"></span></li>');
        }

        $("li.upload-btn").on("click", function() {
            $("#imgchooser").click();
        });


    });
    // End init pastebin

    // Utils
    var getNick = function() {
        return Candy.Core.getUser().getNick();
    };

    var getCurrentMessageField = function() {
        var roomJid = Candy.View.getCurrent().roomJid;
        return Candy.View.Pane.Room.getPane(roomJid, ".message-form").children(".field");
    }

    var getAllNicks = function() {
        var roomJid = Candy.View.getCurrent().roomJid;
        var room = Candy.Core.getRoom(roomJid);
        var users = [];
        for (user in room.getRoster().getAll()) {
            users.push(user.substr(user.lastIndexOf('/') + 1));
        };
        return users;
    }

    var defaultDomain = '@chat.copyleftgames.org';

    // This function will be executed before sending a message
    var beforeSend = (function(msgdata) {

        // Required for the paste bin
        if (msgdata.message.indexOf("\n") != -1) {
            pastebin.paste(msgdata.message);
            return false;
        }

        // Save message history
        messageHistory.addMessage(msgdata.message);

        // Command execution
        var comm = (msgdata.message.trim() + ' ').split(" ")[0];
        if (commands[comm]) {
            var event = null;
            return commands[comm](event, msgdata);
        }
        // IMPORTANT!
        return true;
    });


    /******************************
    Commands data structure

    Adding commands:
        * Just add a new key (the command) and it corresponding function.
        * Nothing more is necessary. The function will get two arguments:
            event
            msgdata <-- https://candy-chat.github.io/candy/docs/files/view/pane-js.html#candy:view.message.before-send

        * Return true if you want the command to be echoed
           and false otherwise
    ******************************/
    var commands = {
        /**************************************
        Hide/show the roster

        command: /roster [show|hide]
        ****************************************/
        "/roster": function(event, msgdata) {
            var args = msgdata.message.split(" ");

            switch (args[1]) {
                case "show":
                    rosterManagement.show();
                    break;
                case "hide":
                    rosterManagement.hide();
                    break;
                default:
                    rosterManagement.toggle();
            }

            return false;
        },

        /**************************************
        Change nick in chat

        command: /nick <new_nick>
        ****************************************/
        '/nick': function(event, msgdata) {
            var newNick = msgdata.message.slice('/nick'.length + 1);

            Candy.Core.Action.Jabber.SetNickname(newNick);
            requestedNick = newNick;

            getCurrentMessageField().val("").focus();
            return false;
        },


        /*************************************
        Test command, nothing is sent.

        command: /test
        **************************************/
        '/test': function(event, msgdata) {
            console.log("TEST");
            //Candy.View.Pane.Chat.infoMessage(Candy.View.getCurrent().roomJid, '', "");
            return false;
        },

        /*************************************
        Join command to join another room

        command: /join <room_name> <password(optional)>
        **************************************/
        '/join': function(event, msgdata) {
            var msg = msgdata.message.trim();
            var args = msg.substring(msg.indexOf(" "));
            if (args === undefined || args === '' || args === null) {
                //Candy.View.Pane.Chat.infoMessage(Candy.View.getCurrent().roomJid, '', "usage: /join roomname");
            }

            args = args.trim().split(' ');

            var roomJid = args[0];

            if (roomJid === undefined || roomJid === '' || roomJid === null) {
                //Candy.View.Pane.Chat.infoMessage(Candy.View.getCurrent().roomJid, '', "usage: /join roomname");
                return false;
            }

            if (roomJid.indexOf('@') === -1) {
                roomJid = roomJid + defaultDomain;
            }

            var password = args[1];

            Candy.View.Pane.Room.getPane(Candy.View.getCurrent().roomJid, ".message-form")
                .children(".field").val("");

            if (password === undefined || password === '' || password === null) {
                Candy.Core.Action.Jabber.Room.Join(roomJid);
            } else {
                Candy.Core.Action.Jabber.Room.Join(roomJid, password);
            }
            return false;
        },

        /*************************************
        Part command to leave current room

        command: /part
        **************************************/
        '/part': function(event, msgdata) {
            var roomJid = Candy.View.getCurrent().roomJid;
            Candy.Core.Action.Jabber.Room.Leave(roomJid);
            return false;
        },

        /*************************************
        Lists all available commands

        command: /help
        **************************************/

        '/help': function(event, msgdata) {
            for (cmd in commands) {
                //TODO: fix this. Currently gives an error except when the message is an empty string
                //Candy.View.Pane.Chat.infoMessage(Candy.View.getCurrent().roomJid, '', cmd);
                console.log(cmd);
            }
            return false;
        }
    };


    $(Candy).on("candy:view.message.before-send", function(event, msgdata) {
        return beforeSend(msgdata);
    });

    /*******************************
    Nick collisions when joining the room
    ********************************/
    var requestedNick = null;
    $(Candy).on('candy:core.presence.error', function(event, args) {
        // On name conflict error
        // user will first rejoin room with old nick
        // then when he rejoins nickname will change to newNick as modified below (refer to candy:core.presence.room)
        if (args.type === 'conflict') {
            var nick = getNick();
            if (requestedNick !== null) {
                nick = requestedNick;
            }

            var newNick = '';
            if (/.*\([0-9]+\)/.test(nick)) {
                var index = parseInt(nick.substring(nick.lastIndexOf('(') + 1, nick.lastIndexOf(')')));
                index++;
                newNick = nick.substring(0, nick.lastIndexOf('(') + 1) + index + ')';
            } else {
                newNick = nick + ' (1)';
            }

            requestedNick = newNick;

            Candy.Core.Action.Jabber.Room.Join('gci@chat.copyleftgames.org');
        }
    });

    // Disable nick conflict dialog
    $(Candy).on('candy:core.before-connect', function() {
        $(Candy).unbind('candy:core.presence.error', Candy.View.Observer.PresenceError);
    });



    /**************************
    Handling pressing keys
    ***************************/
    $(document).on('keydown', 'textarea[name="message"]:visible', function(event) {
        switch (event.which) {

            // Tab key
            case 9:
                tabCompletion.tab(event);
                break;

                // Up arrow
            case 38:
                messageHistory.upKey(event);
                break;

                // Down arrow
            case 40:
                messageHistory.downKey(event);
                break;
        }

        if (event.which !== 9) {
            tabCompletion.nonTab(event);
        }
    });


    /*****************************
    Highlighting mentions
    ******************************/
    $(Candy).on("candy:view.message.before-render", function(event, args) {
        //if <nick>: present in the message and it's not from myself, highlight it
        if (args.templateData.message.indexOf(getNick() + ":") !== -1) {
            if (args.templateData.name !== getNick()) {
                args.templateData.message = '<span class="webchat_message_highlight">' + args.templateData.message + "</span>";
                args.template = args.template.replace('{{displayName}}', '<span class="webchat_name_highlight">{{displayName}}</span>');
                Candy.View.Pane.Chat.Toolbar.playSound();
            }
        }
    });


    /*****************************
    Changing own nickname locally on recieving presence from the server
    ******************************/
    $(Candy).on("candy:core.presence.room", function(event, args) {
        if (args.action === "nickchange") {
            if (args.currentUser === args.user) {
                Candy.Core.getUser().setNick(args.currentUser.getNick());
                Candy.Core.getUser().setPreviousNick(args.currentUser.getPreviousNick());
                Candy.Core.getUser().setCustomData(args.currentUser.getCustomData());
                localStorage.setItem("webchatNick", getNick());
            }
        }
        if (args.action === "join") {
            if (requestedNick != null) {
                console.log('Conflicting nick, changing to: ' + requestedNick);
                Candy.Core.Action.Jabber.SetNickname(requestedNick);
                requestedNick = null;
            }
        }
    });


    /***************************************
    Tab completion handler object

    Methods:
      tab(event):     Called when tab is pressed
      nonTab(event):  Called when everything else is pressed
    ****************************************/
    var tabCompletion = (function() {
        function getSuggestions(startSeq) {

            //Add all commands
            var suggestions = Object.keys(commands).map(function(val) {
                return val + ' '
            });

            // Add all nicks
            var room = Candy.Core.getRoom(Candy.View.getCurrent().roomJid);
            if (room !== null) {
                var occupants = room.getRoster().getAll();
                for (var user in occupants) {
                    suggestions.push(user.substr(user.lastIndexOf('/') + 1) + ': ');
                }
            }

            //Remove unwanted
            startSeq = startSeq.toLowerCase();
            suggestions = suggestions.filter(function(elem, index) {
                return elem.toLowerCase().startsWith(startSeq);
            }).sort();

            return suggestions;
        }

        var lastWord = null;
        var lastNameIndex = -1;
        var lastSpaceIndex = -1;

        var tabCompletion = {};

        tabCompletion.tab = function(event) {
            // Prevent moving focus to the send button
            event.preventDefault();

            var $message = $('textarea[name="message"]');
            var text = $message.val();

            lastSpaceIndex = (lastSpaceIndex === -1 ? text.lastIndexOf(' ') + 1 : lastSpaceIndex);
            lastWord = (lastWord === null ? text.substr(lastSpaceIndex) : lastWord);
            lastNameIndex = lastNameIndex + 1;

            var suggestions = getSuggestions(lastWord);

            if (suggestions.length > 0) {
                if (lastNameIndex >= suggestions.length) lastNameIndex = 0;

                var newMessage = text.substring(0, lastSpaceIndex) + suggestions[lastNameIndex];
                $message.val(newMessage);
            }
        };

        tabCompletion.nonTab = function(event) {
            // If the pressed key is not Tab, reset variables to default.
            lastWord = null;
            lastSpaceIndex = -1;
            lastNameIndex = -1;
        };

        return tabCompletion;
    })();



    /***************************************
    Message history handler object

    Variables:
      options.messageHistoryLimit
                            Maximum number of saved messages
    Methods:
      upKey(event):         Called when up arrow is pressed
      downKey(event):       Called when down arrow is pressed
      addMessage(message):  Add message to the buffer
    ****************************************/
    var messageHistory = (function() {
        var messageHistory = {};

        var msgQueue = [];
        var currMsgIndex = 0;

        messageHistory.options = {
            messageHistoryLimit: (25 + 1) //+1th value is empty
        };

        messageHistory.addMessage = function(message) {
            if (msgQueue.length === messageHistory.options.messageHistoryLimit) {
                msgQueue.pop();
            }
            msgQueue.unshift(message);
        };

        messageHistory.upKey = function(e) {
            e.preventDefault();
            //make sure we're within our limits ;p
            if (currMsgIndex < messageHistory.options.messageHistoryLimit && currMsgIndex < msgQueue.length) {
                currMsgIndex++;

                //set value
                getCurrentMessageField().val(msgQueue[currMsgIndex - 1]).focus();
            }
        };

        messageHistory.downKey = function(e) {
            if (currMsgIndex > 0) {
                currMsgIndex--;
            } else {
                getCurrentMessageField().val('');
                return;
            }

            getCurrentMessageField().val(msgQueue[currMsgIndex - 1]).focus();
        };

        return messageHistory;
    })();

    /***************************************
    Roster hide/show handling

    Methods:
      show():   Shows the roster
      hide():   Hides the roster
      toggle(): Toggles the roster visibility (shows if hidden and hides if shown)
    ****************************************/
    var rosterManagement = (function() {
        var mgmt = {};

        mgmt.show = function() {
            $("#chat-pane").removeClass("roster-hidden");
        };
        mgmt.hide = function() {
            $("#chat-pane").addClass("roster-hidden");
        };
        mgmt.toggle = function() {
            $("#chat-pane").toggleClass("roster-hidden")
        };

        // init stuff

        $(Candy).on("candy:view.room.after-add", function() {
            // adds toggle handles whenever a new room is loaded (a bit hacky, I know)
            $(".room-pane").each(function(i, e) {
                if ($(e).find(".roster-toggle").length == 0) {
                    var toggle = document.createElement("div");
                    toggle.className = "roster-toggle";

                    var icon = document.createElement("div");
                    icon.className = "roster-toggle-icon";

                    toggle.appendChild(icon);
                    $(toggle).on("click", mgmt.toggle).on("touchend", mgmt.toggle);
                    e.appendChild(toggle);
                }
            });
        });

        // when the chat is first focused on a mobile device (width < 720), the roster
        // (normally shown) will hide
        var firstfocus = true;
        $("#candy")[0].addEventListener("focus", function() {
            if (firstfocus) {
                firstfocus = false;
                if (window.innerWidth < 720) mgmt.hide();
            }
        }, true);

        return mgmt;
    })();
});

// console code
var consoleManagement = (function() {
    var mgmt = {};
    var self = mgmt;

    mgmt.showing = false;

    mgmt.init = function() {
        var candyContainer = $('#candycontainer');
        var elem = $('#candy');

        // container
        elem.after('<div id="consolecontainer"></div>');
        // description
        elem.after('<h3 id="console-show">Console &#x25BC;</h3>');
        elem.after('<h3 id="console-hide">Console &#x25B2;</h3>');

        // showing and hiding the console
        $('#console-hide').hide();
        $('#consolecontainer').hide();

        $('#console-show').click(function() {
            self.show();
        });

        $('#console-hide').click(function() {
            self.hide();
        });


        var consoleContainer = $('#consolecontainer');
        // debug logs
        consoleContainer.append('<div id="console-log"></div>');
        // textarea for inputting code
        consoleContainer.append('<textarea id="console-input" rows="12" placeholder="XMPP Code"></textarea>');
        // submit button
        consoleContainer.append('<button id="console-submit">Send</button>')
            // error message
        consoleContainer.append('<p id="console-error"></p>')

        // override logging
        Candy.Core.log = function(msg) {
            appendMsg(msg);
        }

        // submit
        $('#console-submit').click(function() {
            var xml = $('#console-input').val();

            // set empty error message
            $('#console-error').text("");

            // if textarea is empty do not do anything
            if (xml == "") {
                return null;
            }

            sendRawXML(xml);

            $('#console-input').val("");
        });
    };

    mgmt.show = function() {
        $('#console-show').hide();
        $('#console-hide').show();
        $('#consolecontainer').show("fast", function() {
            $('#console-log').scrollTop($('#console-log').prop("scrollHeight"));
        });
        self.showing = true;
    }

    mgmt.hide = function() {
        $('#console-hide').hide();
        $('#console-show').show();
        $('#consolecontainer').hide("fast");
        self.showing = false;
    }

    mgmt.toggle = function() {
        if (self.showing)
            self.hide();
        else
            self.show();
    }

    // send raw xml to server
    var sendRawXML = function(xml) {
        // get parsed xml
        // will show error under console and avoid sending if there is any
        var parsed = parseXML(xml);
        if (parsed == null)
            return;

        // get stanza name
        var name = getNodeName(parsed);

        // creating xmlELement for sending because strophe doesn't have function for sending raw xml code
        var xmlElement = createXMLElement(parsed);

        // 3 major stanza types will be handled, and if unknown stanza is given will just send to the server
        switch (name) {
            case "message":
                Candy.Core.getConnection().send(xmlElement);
                break;
            case "presence":
                Candy.Core.getConnection().send(xmlElement);
                break;
            case "iq":
                Candy.Core.getConnection().sendIQ(
                    xmlElement,
                    function() {
                        console.log("Successfully sent IQ stanza:\n" + xml);
                    },
                    function() {
                        var error = "Timed out. Failed to send IQ stanza:\n" + xml;
                        console.log(error);
                        $('#console-error').text(error);
                    },
                    5000);
                break;
            default:
                Candy.Core.getConnection().send(xmlElement);
        }
    }

    // append msg to console
    var appendMsg = function(msg) {
        var consolelog = $('#console-log');

        var dir = msg.substring(0, 4);
        var dircss;
        if (dir == "SENT") {
            msg = msg.substring(5);
            dircss = "console-log-sent";
        } else if (dir == 'RECV') {
            msg = msg.substring(5);
            dircss = "console-log-recv";
        } else
            dircss = "console-log-misc";

        var log = $('<div class="console-log-msg ' + dircss + '"></div>');
        msg = beautifyXml(msg);
        msg = msg.replace("\n", "");
        msg = msg.trim();
        log.text(msg);

        consolelog.append(log);
        // scroll to bottom
        consolelog.scrollTop(consolelog.prop("scrollHeight"));
    }

    // parseXML
    var parseXML = function(xml) {
        $('#console-error').text("");

        try {
            var parsed = $.parseXML(xml);
            return $(parsed)[0].childNodes[0];
        } catch (e) {
            $('#console-error').text(e.message);
        }
        return null;
    }

    // get first node
    var getNodeName = function(parsed) {
        return parsed.nodeName;
    };

    // create xmlElement from string which is required to for Strophe to send
    var createXMLElement = function(parsed) {
        var name = getNodeName(parsed);
        var attrs = getAttrs(parsed);

        var builder = new Strophe.Builder(name, attrs);

        builder = addChildren(builder, parsed.childNodes);

        return builder.tree();
    };

    var addChildren = function(builder, nodeList) {
        for (var i = 0; i < nodeList.length; i++) {
            var child = nodeList[i];
            if (child.nodeName == "#text")
                builder.t(child.nodeValue);
            else {
                var childNode = createXMLElement(child);
                builder.cnode(childNode).up();
            }
        }
        return builder;
    };

    var getAttrs = function(parsed) {
        var attributes = parsed.attributes;
        if (attributes == null)
            return {};
        var attr = {};
        for (var i = 0; i < attributes.length; i++) {
            var a = attributes[i];
            var name = a.name;
            var value = a.value;
            attr[name] = value;
        }
        return attr;
    };

    return mgmt;
})();

/**
 * Code for beautifying xml before showing in console
 **/

// helper
var createShiftArr = function(step) {

    var space = '    ';

    if (isNaN(parseInt(step))) { // argument is string
        space = step;
    } else { // argument is integer
        switch (step) {
            case 1:
                space = ' ';
                break;
            case 2:
                space = '  ';
                break;
            case 3:
                space = '   ';
                break;
            case 4:
                space = '    ';
                break;
            case 5:
                space = '     ';
                break;
            case 6:
                space = '      ';
                break;
            case 7:
                space = '       ';
                break;
            case 8:
                space = '        ';
                break;
            case 9:
                space = '         ';
                break;
            case 10:
                space = '          ';
                break;
            case 11:
                space = '           ';
                break;
            case 12:
                space = '            ';
                break;
        }
    }

    var shift = ['\n']; // array of shifts
    for (ix = 0; ix < 100; ix++) {
        shift.push(shift[ix] + space);
    }
    return shift;
};

// beautify
var beautifyXml = function(text, step) {

    var ar = text.replace(/>\s{0,}</g, "><")
        .replace(/</g, "~::~<")
        .replace(/\/>/g, "\></>")
        .replace(/\s*xmlns\:/g, "~::~xmlns:")
        .replace(/\s*xmlns\=/g, "~::~xmlns=")
        .split('~::~'),
        len = ar.length,
        inComment = false,
        deep = 0,
        str = '',
        ix = 0,
        shift = step ? createShiftArr(step) : createShiftArr('    ');

    for (ix = 0; ix < len; ix++) {
        // start comment or <![CDATA[...]]> or <!DOCTYPE //
        if (ar[ix].search(/<!/) > -1) {
            str += shift[deep] + ar[ix];
            inComment = true;
            // end comment  or <![CDATA[...]]> //
            if (ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1 || ar[ix].search(/!DOCTYPE/) > -1) {
                inComment = false;
            }
        } else
        // end comment  or <![CDATA[...]]> //
        if (ar[ix].search(/-->/) > -1 || ar[ix].search(/\]>/) > -1) {
            str += ar[ix];
            inComment = false;
        } else
        // <elm></elm> //
        if (/^<\w/.exec(ar[ix - 1]) && /^<\/\w/.exec(ar[ix]) &&
            /^<[\w:\-\.\,]+/.exec(ar[ix - 1]) == /^<\/[\w:\-\.\,]+/.exec(ar[ix])[0].replace('/', '')) {
            str += ar[ix];
            if (!inComment) deep--;
        } else
        // <elm> //
        if (ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) == -1 && ar[ix].search(/\/>/) == -1) {
            str = !inComment ? str += shift[deep++] + ar[ix] : str += ar[ix];
        } else
        // <elm>...</elm> //
        if (ar[ix].search(/<\w/) > -1 && ar[ix].search(/<\//) > -1) {
            str = !inComment ? str += shift[deep] + ar[ix] : str += ar[ix];
        } else
        // </elm> //
        if (ar[ix].search(/<\//) > -1) {
            str = !inComment ? str += shift[--deep] + ar[ix] : str += ar[ix];
        } else
        // <elm/> //
        if (ar[ix].search(/\/>/) > -1) {
            str = !inComment ? str += shift[deep] + ar[ix] : str += ar[ix];
        } else
        // <? xml ... ?> //
        if (ar[ix].search(/<\?/) > -1) {
            str += shift[deep] + ar[ix];
        } else
        // xmlns //
        if (ar[ix].search(/xmlns\:/) > -1 || ar[ix].search(/xmlns\=/) > -1) {
            str += shift[deep] + ar[ix];
        } else {
            str += ar[ix];
        }
    }

    str = str.replace(/\><\/>/g, "/>");
    return (str[0] == '\n') ? str.slice(1) : str;
}
