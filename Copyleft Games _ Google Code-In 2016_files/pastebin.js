var pastebin = {};
pastebin.ready = false;


pastebin.init = (function(a, b) {
    pastebin.ready = true;
    pastebin.codeURL = a;
    pastebin.imgURL = b;
});


pastebin.sendMessage = (function(text) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initiased.");
    }
    var jid = Candy.View.getCurrent().roomJid;
    var type = Candy.View.Pane.Chat.rooms[jid].type;
    Candy.Core.Action.Jabber.Room.Message(jid, text, type);
});


pastebin.sendRichMessage = (function(media, message, html_message) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }
    var connection = Candy.Core.getConnection()
    var room_nick = Candy.View.getCurrent().roomJid;
    var type = Candy.View.Pane.Chat.rooms[room_nick].type;
    // Copied from Strophe -- Arc
    var msg, msgid, parent;
    type = type || (nick != null ? "chat" : "groupchat");
    msgid = connection.getUniqueId();
    msg = $msg({
        to: room_nick,
        from: connection.jid,
        type: type,
        id: msgid
    }).c("body", {
        xmlns: Strophe.NS.CLIENT
    }).t(message);
    msg.up();

    msg.c("media", {
        xmlns: "urn:xmpp:message:multimedia"
    });

    $(media).children("source").each(function() {
        msg.c("source", {
            src: $(this).attr("src"),
            type: $(this).attr("type")
        });
        msg.up();
    });
    msg.up();
    console.log(media);
    console.log(msg.toString());
    if (html_message != null) {
        msg.c("html", {
            xmlns: Strophe.NS.XHTML_IM
        }).c("body", {
            xmlns: Strophe.NS.XHTML
        }).h(html_message);
        if (msg.node.childNodes.length === 0) {
            parent = msg.node.parentNode;
            msg.up().up();
        }
    }
    msg.c("x", {
        xmlns: "jabber:x:event"
    }).c("composing");
    connection.send(msg);
    // End copy
});

pastebin.paste = (function(e) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }

    $('textarea[name="message"]').val("");
    $.ajax({
        method: "POST",
        url: this.codeURL,
        data: { content: e }
    }).done(function(msg) {
        var xmlDoc = $(msg);
        var media = xmlDoc.find("media");
        var plain = xmlDoc.find("plaintext");
        var rich = xmlDoc.find("body");
        console.log(media.html());
        pastebin.sendRichMessage(media, plain.html(), rich.html());
  });
});


pastebin.pasteImage = (function(file) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }
    var fd = new FormData();
    console.log(file.type);
    if (file.type === "image/png")
        fd.append("type", ".png");
    else if (file.type === "image/jpeg")
        fd.append("type", ".jpg");
    else throw Error("Unsupported file type");

    fd.append("content", file.slice());
    var req = new XMLHttpRequest();
    req.overrideMimeType("multipart/form-data");
    req.open("post", pastebin.imgURL);
    req.onreadystatechange = (function() {
        if (req.readyState == 4 && req.status == 200) {
            console.log(req.response);
            var xmlDoc = $($.parseXML(req.response));
            var media = xmlDoc.find("media");
            var plain = xmlDoc.find("plaintext");
            var rich = xmlDoc.find("body");
            pastebin.sendRichMessage(media, plain.html(), rich.html());
        }
    });
    req.send(fd);
});
