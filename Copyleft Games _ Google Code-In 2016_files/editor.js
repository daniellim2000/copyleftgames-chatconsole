var sessions = [];
var editor = ace.edit("editor-ace");
editor.setOptions({
    maxLines: Infinity
});
editor.setTheme("ace/theme/dawn");

var modelist = ace.require("ace/ext/modelist");

editor.getSession().setMode("ace/mode/javascript");

var languageDropdown = document.getElementById("editor-language");
var modeFromFileName = null;

var defaultTitle = "Untitled Document";
var previousTab = 0;
var nonEditorTabCount = 5;

function getLanguage() {
    return languageDropdown.options[languageDropdown.selectedIndex].value;
}

function getMode() {
    var language = getLanguage();
    return "ace/mode/" + language;
}

function updateLanguage() {
    var mode = getMode();
    if (modeFromFileName) {
        if (mode == "ace/mode/custom") {return;}
        removeCustomOption();
        modeFromFileName = null; // assuming a dropdown selection to override current mode
    }
    editor.getSession().setMode(mode);
    editor.focus();
}

updateLanguage();

languageDropdown.addEventListener("change", updateLanguage);

var useSpaces = document.getElementById("spaces");
var spaces2 = document.getElementById("spaces-2");
var spaces4 = document.getElementById("spaces-4");
var spaces8 = document.getElementById("spaces-8");
var button = document.getElementById("indent-settings-button");
var div = document.getElementById("indent-settings");
var shown = false;

function updateUseSpaces() {
    editor.getSession().setUseSoftTabs(useSpaces.checked);
}

function updateTabLength() {
    var tabSize;
    if (spaces2.checked) {
        tabSize = 2;
    } else if (spaces4.checked) {
        tabSize = 4;
    } else {
        tabSize = 8;
    }
    editor.getSession().setTabSize(tabSize);
}

function toggleDiv() {
    if (shown) {
        div.style.display = "none";
        shown = false;
    } else {
        div.style.display = "block";
        shown = true;
    }
}

function closeDiv() {
    div.style.display = "none";
    shown = false;
}

function focus(e) {
    var id = e.target.id;
    if (id == "spaces-2" || id == "spaces-4" || id == "spaces-8" || id == "indent-settings" || id == "spaces" || id == "indent-settings-button") {
        // clicked on an indent setting
        return;
    } else if (id == "editor-language") {
        // clicked on the editor language dropdown
        return;
    }
    if (shown) {
        // the settings panel is open, close it
        closeDiv();
    }
    editor.focus();
}

updateUseSpaces();
updateTabLength();

useSpaces.addEventListener("change", updateUseSpaces);
spaces2.addEventListener("change", updateTabLength);
spaces4.addEventListener("change", updateTabLength);
spaces8.addEventListener("change", updateTabLength);
button.addEventListener("click", toggleDiv);

var newTab = document.getElementById("new-tab");
var tabList = document.getElementById("navmenu");

document.getElementById("editor-panel").addEventListener("click", focus);

// Stub function to be updated by tab implementation task
// Note that this will need to handle the options, probably
// by attaching them to the editor sessions for each file
function createTabFromText(filename, text) {
}

var fileExtensions = {
    "c_cpp": "c",
    "css": "css",
    "glsl": "glsl",
    "html": "html",
    "javascript": "js",
    "lua": "lua",
    "python": "py",
    "plain_text": "txt",
    "svg": "svg",
    "vala": "vala",
    "xml": "xml"
};

var mimeTypes = {
    "c_cpp": "text/x-c",
    "css": "text/css",
    "glsl": "text/plain",
    "html": "text/html",
    "javascript": "text/javascript",
    "lua": "application/x-lua",
    "python": "text/x-script.phyton",
    "plain_text": "text/plain",
    "svg": "text/xml",
    "vala": "text/plain",
    "xml": "text/xml"
};

var mimeTypesExtensions = {
    "c": ["text/x-c","c_cpp"],
    "css": ["text/css","css"],
    "glsl": ["text/plain", "glsl"],
    "html": ["text/html", "html"],
    "js": ["text/javascript", "javascript"],
    "lua": ["application/x-lua", "lua"],
    "py": ["text/x-script.phyton", "python"],
    "txt": ["plain/text", "plain_text"],
    "svg": ["text/xml", "svg"],
    "vala": ["text/plain", "vala"],
    "xml": ["text/xml","xml"]
};

var downloadIcon = document.getElementById('download-icon');
downloadIcon.addEventListener("click", function() {
    var downloadFileName = "Untitled.txt";
    // Getting the text from the editor and converting it to Base64.
    var typeMime = "text/plain"
    var highlightMode = "plain_text";

    function downloadDataUrlFromJavascript(filename, dataUrl) {
        // Construct the a element
        var link = document.createElement("a");
        link.download = filename;
        link.target = "_blank";

        // Construct the uri
        link.href = dataUrl;
        document.body.appendChild(link);
        link.click();

        // Cleanup the DOM
        document.body.removeChild(link);
        delete link;
    }
    if (sessions[previousTab]["title"] == undefined || sessions[previousTab]["title"] == defaultTitle){
    //Getting the name of the file. Need Better prompt....
        downloadFileName = window.prompt("What's the name of the file?");
	if (!downloadFileName){
            downloadFileName = "Untitled.txt"
        }
    } else {
        downloadFileName = sessions[previousTab]["title"];
    }
    if (downloadFileName.split(".").length > 1){
        if (downloadFileName.split(".")[1] in mimeTypesExtensions){
            typeMime = mimeTypesExtensions[downloadFileName.split(".")[1]][0];
            highlightMode = mimeTypesExtensions[downloadFileName.split(".")[1]][1];
        }
    }
    var dataURL = 'data:' + typeMime + ';base64,' + window.btoa(editor.getValue());
    downloadDataUrlFromJavascript(downloadFileName,dataURL);
    editor.getSession().setMode("ace/mode/"+highlightMode);
    sessions[previousTab]["title"] = downloadFileName;
    document.getElementById("editor_"+sessions[previousTab]["id"]).innerHTML = document.getElementById("editor_"+sessions[previousTab]["id"]).innerHTML.replace(/n.*s/, "n> "+downloadFileName.split(".")[0] +" <s");
});

// 5 MiB is quite enough for a text based file thanks
var fileSizeLimit = (5 * 1024 * 1024);

function handleUpload(event) {
    var fileList = event.target.files;
    for (var i = 0; i < fileList.length; i++) {
        var fileEntry = fileList[i];
        if (fileEntry.size > fileSizeLimit) {
            // Skip with error
            console.error("Encountered oversized file. File had ", fileEntry.size, " bytes but limit is ", fileSizeLimit, " bytes");
            continue;
        }
        // Create an async reader which will deposit the text into the tab
        var fileName = fileEntry.name;
        var reader = new FileReader();
        reader.onload = function () {
            var lineConverted = reader.result.replace(/\r\n/g, "\n");
            var splitLines = lineConverted.split("\n");
            openNewTab(fileName, splitLines);
        };
        reader.readAsText(fileEntry);
        break;
    }
}

document.getElementById("file-select").addEventListener("change", handleUpload);

function saveFile() {
    var fileName = window.prompt("What would you like to save your file as?\n\nNOTE: The save function in the editor is NOT YET SUPPORTED and is being worked on.");
    if (fileName) {
        modeFromFileName = modelist.getModeForPath(fileName);
        editor.getSession().setMode(modeFromFileName.mode);
        addCustomOption(modeFromFileName);
        editor.focus();
    }
}

function addCustomOption(mode) {
    if (!document.getElementById('language-custom')){
        var option = document.createElement("option");
        option.innerHTML = mode.caption;
        option.value = "custom";
        option.id = "language-custom";
        languageDropdown.appendChild(option);
        languageDropdown.selectedIndex = languageDropdown.options.length - 1;
    } else {
        document.getElementById('language-custom').innerHTML = mode.caption;
    }
}

function removeCustomOption() {
    document.getElementById('language-custom').remove();
}

document.getElementById('save-button').addEventListener('click', saveFile);

function openNewTabListener(event){
    openNewTab();
}

function setCurrentListener(event){
    setCurrent(event.target.parentNode.getAttribute("id"), event.target.title);
}

function removeTabListener(event){
    removeTab(event.target.parentNode.getAttribute("id").split("_")[1]);
}


function setCurrent(ID, title, remove){
    var index = ID.split("_")[1];

    if(previousTab !== index || (previousTab === index && remove === true)){
        var previousTabNode = tabList.childNodes[parseInt(previousTab)+nonEditorTabCount];
        previousTabNode.setAttribute("class", "editor_inactiveTab");
        cross = previousTabNode.getElementsByTagName("a")[0];

        if(cross !== undefined){
            previousTabNode.removeChild(cross);
        }

        activeTabLi = document.getElementById(ID);
        if(activeTabLi === null){
            console.log("Error setting current tab, getElementById returned null");
            return;
        }

        //don't add close button if only one tab is left
        if(sessions.length > 1){
            var closeIcon = document.createElement("a");
            closeIcon.setAttribute("href", "#editor");
            closeIcon.setAttribute("class", "editor_closeButton");
            closeIcon.innerHTML = "x";
            closeIcon.addEventListener("click", removeTabListener);
            activeTabLi.appendChild(closeIcon);
        }
        activeTabLi.setAttribute("class", "editor_activeTab");

        //tabList.replaceChild(activeTabLi, tabList.childNodes[index+5]);

        editor.setSession(sessions[index].sess);
        editor.resize();
        editor.focus();

        previousTab = index;
    }
}


function openNewTab(title, content){
    var session;
    var remTab = false;
    if(sessions.length == 1 && editor.getValue() == "" && (sessions[previousTab]["title"] == undefined || sessions[previousTab]["title"] == defaultTitle)){
        remTab = true;
    }
    if(content === undefined || content === null || content === ""){
        session = new ace.EditSession("");
    }else{
        session = new ace.EditSession(content);
    }

    editor.setSession(session);
    editor.resize();
    editor.focus();
    var index = sessions.length;
    sessions.push({
        sess: session,
        title: title,
        id: index
    });

    var tabLi = document.createElement("li");
    var titleSpan = document.createElement("span");
		var editorIcon = document.createElement("i");
		editorIcon.setAttribute("class", "material-icons")
		editorIcon.innerHTML = "mode_edit";//<i class="material-icons">mode_edit</i>
    if(title){
        titleSpan.innerHTML = title;
    }else{
        titleSpan.innerHTML = defaultTitle;
    }
    tabLi.setAttribute("class", "editor_inactiveTab");
    tabLi.setAttribute("id", "editor_" + index);
    titleSpan.addEventListener("click", setCurrentListener);
		tabLi.appendChild(editorIcon);
    tabLi.appendChild(titleSpan);

    tabList.appendChild(tabLi);
    setCurrent("editor_" + index, title, false);
    window.location.href = "#editor";
    if(remTab){
        removeTab(0);
    }
}

function removeTab(index){
    tabList.removeChild(tabList.childNodes[parseInt(index)+nonEditorTabCount]);
    sessions.splice(index, 1);
    nodes = tabList.childNodes;
    for(var i = index;i < sessions.length; i++){
        var node = nodes[parseInt(i)+nonEditorTabCount];
        node.setAttribute("id", "editor_" + i);
        tabList.replaceChild(node, nodes[parseInt(i)+nonEditorTabCount]);
        sessions[i].id = sessions[i].id - 1;
    }
		console.log(sessions);
    if(index <= previousTab){
        previousTab = Math.max(previousTab-1, 0);
    }
    tempSession = sessions[0];
    setCurrent("editor_" + tempSession.id, tempSession.title, true);
}

openNewTab(defaultTitle, "");
newTab.addEventListener("click", openNewTabListener);
