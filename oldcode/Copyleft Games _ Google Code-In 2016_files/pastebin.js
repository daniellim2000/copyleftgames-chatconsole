var pastebin = {};
pastebin.ready = false;


pastebin.init = (function(a, b) {
    pastebin.ready = true;
    pastebin.codeURL = a;
    pastebin.imgURL = b;
});


pastebin.sendMessage = (function(text) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initiased.");
    }
    var jid = Candy.View.getCurrent().roomJid;
    var type = Candy.View.Pane.Chat.rooms[jid].type;
    Candy.Core.Action.Jabber.Room.Message(jid, text, type);
});


pastebin.sendRichMessage = (function(plain, rich) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }
    var jid = Candy.View.getCurrent().roomJid;
    var type = Candy.View.Pane.Chat.rooms[jid].type;
    Candy.Core.Action.Jabber.Room.Message(jid, plain, type, rich);
});

pastebin.paste = (function(e) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }

    $('textarea[name="message"]').val("");
    $.ajax({
        method: "POST",
        url: this.codeURL,
        data: { content: e }
    }).done(function(msg) {
        var xmlDoc = $(msg);
        var plain = xmlDoc.find("plaintext");
        var rich = xmlDoc.find("body");
        pastebin.sendRichMessage(plain.html(), rich.html());
  });
});


pastebin.pasteImage = (function(file) {
    if (!pastebin.ready) {
        throw Error("Pastebin not initialised.");
    }
    var fd = new FormData();
    console.log(file.type);
    if (file.type === "image/png")
        fd.append("type", ".png");
    else if (file.type === "image/jpeg")
        fd.append("type", ".jpg");
    else throw Error("Unsupported file type");

    fd.append("content", file.slice());
    var req = new XMLHttpRequest();
    req.overrideMimeType("multipart/form-data");
    req.open("post", pastebin.imgURL);
    req.onreadystatechange = (function() {
        if (req.readyState == 4 && req.status == 200) {
            console.log(req.response);
            var xmlDoc = $($.parseXML(req.response));
            var plain = xmlDoc.find("plaintext");
            var rich = xmlDoc.find("body");
            pastebin.sendRichMessage(plain.html(), rich.html());
        }
    });
    req.send(fd);
});
